classdef ppSequence < mcmcBayes.sequence
    %PPSEQUENCE
    %   Power posterior sequence that includes a set of simulations that implement power-posterior 
    % estimation of the marginal likelihood of a model given a dataset.

    properties
        loglikelihoodMean           %array(temperatures,chains) of mean of log-likelihood of the data given the model (across all the samples)
        loglikelihoodVar            %array(temperatures,chains) of variance of log-likelihood of the data given the model (across all the samples)
        loglikelihoodMce            %array(temperatures,chains) of Monte Carlo error of log-likelihood of the data given the model (across all the samples)
        ppN                         %Power-posterior method's number of temperature iterations to perform for marginal loglikelihood estimation (Friel et al. 2008)
        ppK                         %Power-posterior method's near zero compression factor (ensures higher sampling entropy nearer to the prior) (Friel et al. 2008)        
        ppMarginalLogLikelihood     %array(chains) of marginal loglikelihood of posterior given data that is computed using power-posterior method (Friel et al. 2008)
        ppMarginalLogLikelihoodAlt  %array(chains)
        ppKlDistance                %array(chains)
        ppMce                       %array(chains) of total monte carlo error for the power-posterior estimate (Friel et al. 2008)
    end
    
    methods (Static)
        function setSequenceUpdated=updateStructFromXMLNode(rootNode,setSequence)  
            %updateStructFromXMLNode
            %   Worker function that parses and returns information within a structure that represents a root element.  Function can also be called
            %with a sparse structure, that only contains some (and not all) fields in the root element (so this function must handle missing entries).
            stdout=1;
            fprintf(stdout, '\tLoading ppSequence fields from rootNode\n');
            
            setSequenceUpdated=setSequence;

            if (strcmp(rootNode.getNodeName,'ppSequenceN')==1)
                setSequenceUpdated.ppSequenceN=str2num(cast(rootNode.getTextContent(),'char'));
            elseif (strcmp(rootNode.getNodeName,'ppSequenceK')==1)
                setSequenceUpdated.ppSequenceK=str2num(cast(rootNode.getTextContent(),'char'));
            else
                warning('Meed to parse %s *.xml\n',cast(rootNode.getNodeName,'char'));
            end
        end
    end

    methods
        function obj = ppSequence(setSequence)
            %PPSEQUENCE 
            %   Construct an instance of this class
            if nargin > 0
                superargs{1}='mcmcBayes.ppSequence';
                superargs{2}=setSequence.uuid;
                superargs{3}=mcmcBayes.model.constructor(setSequence.model);
                %construct the simulations array (ppSequenceN+1 simulations)
                for tempid=1:setSequence.ppSequenceN+1
                    setSimulations(tempid,1)=mcmcInterface.simulation.constructor(setSequence.simulations(1,1));
                end
                superargs{4}=setSimulations;
            elseif nargin == 0
                superargs={};
            end
            obj=obj@mcmcBayes.sequence(superargs{:});

            obj.ppMarginalLogLikelihood=zeros(obj.simulations(1,1).hSamplerInterfaces.numSamplingChains,1);
            obj.ppMarginalLogLikelihoodAlt=zeros(obj.simulations(1,1).hSamplerInterfaces.numSamplingChains,1);
            obj.ppKlDistance=zeros(obj.simulations(1,1).hSamplerInterfaces.numSamplingChains,1);
            obj.ppMce=zeros(obj.simulations(1,1).hSamplerInterfaces.numSamplingChains,1);
            obj.ppN=setSequence.ppSequenceN;
            obj.ppK=setSequence.ppSequenceK;

            %need to create phi variable for each temp
            for tempid=1:setSequence.ppSequenceN+1
                phi=obj.getPhi(tempid);

                %create an element for phi and insert into into obj.simulations(tempid,1).variables.elements and obj.simulations(tempid,1).samples.elements arrays
                setUuid='?';
                setName='phi';
                setDescription='exponent for powerposterior temperature iteration';
                setGroup='?';
                setDistribution=mcmcInterface.distribution('mcmcInterface.t_elementDistribution.fixed','');
                setDimensions='mcmcInterface.t_valuesDimensions.scalar';
                setType='double';
                setLowerBound=0;
                setUpperBound=1.0;
                setPrecision=1e-5;
                setConstant=phi;
                setGenerated=double.empty;
                setValues=mcmcInterface.values(setDimensions,setType,setLowerBound,setUpperBound,setPrecision,setConstant,setGenerated);
                setStatistics=mcmcInterface.statistics.empty;
                phiElement=mcmcInterface.element(setUuid,setName,setDescription,setGroup,setDistribution,setValues,setStatistics);
                obj.simulations(tempid,1).variables.elements(end+1,1)=phiElement;
                obj.simulations(tempid,1).samples.elements(end+1,1)=phiElement;
                if tempid>1 && tempid<setSequence.ppSequenceN+1
                    obj.simulations(tempid,1).enableMcmcDiagnosticPlots=false;%disable diagnostic plots unless tempid=1 (prior) or =setSequence.ppSequenceN+1 (posterior)
                end
            end            
        end

        function phi = getPhi(obj, tempid)
            %GETPHI
            %   Returns the power-posterior value phi (i.e., the current discretization step), for the specified temperature identifier
            % Note that prior distribution's phi=0 and posterior distribution's phi=1
            ppN=obj.ppN;
            ppK=obj.ppK;
            phi=((cast(tempid,'double')-1)/cast(ppN,'double'))^cast(ppK,'double');%setup phi
        end 

        % mcmcBayes.sequence class declares this function as abstract and its children must implement it
        function retObj = runSequence(obj)
            %RUNSEQUENCE
            %   Runs a set of simulations that is needed to estimate the marginal likelihood via power-posteriors
            stdout=1;

            retObj=obj;
            for tempid=1:obj.ppN+1
                fprintf(stdout,'Running simulation for power-posterior tempid=%d\n',tempid);
                retObj.simulations(tempid,1)=retObj.simulations(tempid,1).runSimulation(obj.hModel);
            end
            retObj=retObj.computeMargLogLikelihood();
        end

        function [retObj]=computeMargLogLikelihood(obj)
            %COMPUTEMARGLOGLIKELIHOOD
            %   Performs the sampling sequence to estimate the marginal loglikelihood of the data given the model 
            %   using the power posterior method (Friel et al. 2008).
            %
            %   The joint likelihood of the data vector for each of the power posterior temperature iterations 
            %   (i.e., pr(data|phi,thetahat)) must be computed prior to estimating the likelihood's mean and variance for 
            %   each temperature sample set. It is incorrect to compute pr(data|phi,thetahat) separately for each data 
            %   point in the set and combine afterwards.
            %
            %   The marginal loglikelihood method requires computing, for each value of phi, log(pr(data|phi,thetahat)) 
            %   for every sample.  These values are used to derive a mean, and the set of n+1 means (one for each
            %   value of phi) is used with the trapezoidal rule to approximate the log-likelihood integral across
            %   phi=[0,1].  Unfortunately, for discrete models based on Poisson processes (HPP, Nhpp), it
            %   is very likely that in a large sample set from log(pr(data|phi,thetahat)), the likelihood for at least
            %   one of the samples will be 0.  This results in a log-likelihood for that sample being -Infinity and is 
            %   obviously problematic for this application, as it causes the mean of the log-likelihood for that value 
            %   of phi to also be -Infinity.  
            %
            %   Alternately, this method computes the likelihood, or pr(data|phi,thetahat), for evey sample.  In the
            %   natural domain, the samples having zero likelihood do not cause the mean of the likelihoods for 
            %   the respective sample set to be -Infinity.  Furthermore, it is not likely that for every sample in
            %   the set the likelihood will be zero (causing the mean of the likelihoods to be zero).  Consequently, 
            %   conversion back to the log-domain by taking the log of each of the n+1 likelihood means, results in a 
            %   finite value.
            %   Input:
            %     obj - Current sequence object
            %   Output:
            %     retob - Updated sequence object
            stdout=1;
            fprintf(stdout,'Computing marginal log-likelihood of the data given the %s model\n','modelnamehere');
                       
            printStatus=true;
            [obj]=obj.computeLogLikelihoods(printStatus);            
            
            for tempid=1:obj.ppN+1
                phiVarElementId=obj.model.getPhiVarElementId();
                phi(tempid)=obj.simulations(tempid,1).variables.elements(phiVarElementId).values.constant;
            end

            for chainid=1:obj.simulations(1,1).hSamplerInterfaces.numSamplingChains
                %ppSequence_marginalLogLikelihood_alt is implementation of the modified ppSequence Friel et al. 2013
                %ppSequence_mce is implementation of Friel et al. 2008
                %use trapezoidal rule to estimate margloglikelihood integral (will have issues with any temperature loglikelihoodMean or loglikelihoodVar being -Inf, Inf, or Nan )                       
                for tempid=1:obj.ppN %1-based alternative to 0:n-1
                    weights(tempid)=phi(tempid+1)-phi(tempid);
                    vals(tempid)=(obj.loglikelihoodMean(tempid+1)+obj.loglikelihoodMean(tempid))/2;
                    bias(tempid)=(1/12)*weights(tempid)^2*(obj.loglikelihoodVar(tempid+1)-obj.loglikelihoodVar(tempid));%temp for margloglikelihood computation
                    %fprintf(1,'for %d, weights=%f,vals=%f\n',tempid,weights(tempid),vals(tempid));
                    if tempid>1
                        mcevals(tempid)=1/2*weights(tempid)^2*obj.loglikelihoodMce(tempid); 
                    else
                        mcevals(tempid)=0;
                    end
                    if isinf(vals(tempid)) || isnan(vals(tempid)) || isinf(bias(tempid)) || isnan(bias(tempid)) || isinf(mcevals(tempid)) || isnan(mcevals(tempid))
                        warning(strcat('There was a margloglikelihood calculation issue for chainid=%d: \n\tloglikelihood_mean(%d)=[%f], \n\tloglikelihood_var(%d)=[%f],',...
                            '\n\tloglikelihood_mean(%d)=[%f], \n\tloglikelihood_var(%d)=[%f], \n\t\n)'),chainid,tempid,obj.loglikelihoodMean(tempid),tempid,...
                            obj.loglikelihoodVar(tempid),tempid+1,obj.loglikelihoodMean(tempid+1),tempid+1,obj.loglikelihoodVar(tempid+1));
                    end
                end
                mcevals(tempid+1)=1/2*(phi(obj.ppN+1)-phi(obj.ppN))^2*obj.loglikelihoodMce(obj.ppN+1);%last computation for mce
                mce=sum(mcevals);
                
                obj.ppMarginalLogLikelihood(chainid,1)=sum(weights.*vals);
                obj.ppMarginalLogLikelihoodAlt(chainid,1)=sum(weights.*vals)-sum(bias);
                obj.ppMce(chainid,1)=mce;
                obj.ppKlDistance(chainid,1)=obj.loglikelihoodMean(obj.ppN+1)-obj.ppMarginalLogLikelihoodAlt;
                dispstr=sprintf('For uuid=%s, chainid=%d, log(pr(data))=%f, alt. log(pr(data))=%f, log(pr(data|theta(t))=%f, mce(log(pr(data|theta(t)))=%f,KL=%f', ...
                    obj.uuid, chainid, obj.ppMarginalLogLikelihood, obj.ppMarginalLogLikelihoodAlt, obj.loglikelihoodMean(obj.ppN+1), obj.ppMce, ...
                    obj.ppKlDistance);
                disp(dispstr);
                setDesktopStatus(dispstr)
            end

            retObj=obj;
        end   

        function [retobj]=computeLogLikelihoods(obj, printStatus)
        % [loglikelihoodMean, loglikelihoodVar, loglikelihoodMCE]=computeLogLikelihoods(obj, printStatus)
        %   Computes the log-likelihoods of the data given the model, for each of the temperature identifiers and returns an array of its mean, variance, and 
        %   Monte carlo error.
            stdout=1;
            
            obj.loglikelihoodMean=zeros(obj.ppN+1,obj.simulations(1,1).hSamplerInterfaces.numSamplingChains);
            obj.loglikelihoodVar=zeros(obj.ppN+1,obj.simulations(1,1).hSamplerInterfaces.numSamplingChains);
            obj.loglikelihoodMce=zeros(obj.ppN+1,obj.simulations(1,1).hSamplerInterfaces.numSamplingChains);
            
            mceNumBatches=obj.simulations(1,1).samples(1).mceNumBatches;

            for tempid=1:obj.ppN+1 %Compute the loglikelihoods of each tempid
                for chainid=1:obj.simulations(1,1).hSamplerInterfaces.numSamplingChains
                    if printStatus
                        fprintf(stdout,'\tComputing likelihood for %s model, chainid=%d, tempid=%d.\n', obj.model.type, chainid, tempid);
                    end
                    
                    try
                        loglikelihood=obj.model.computeLogLikelihood(obj.simulations(tempid,1).data,obj.simulations(tempid,1).samples(chainid));

                        obj.loglikelihoodMean(tempid)=mean(loglikelihood);%although this vector contains log domain, this should be ok for how it is used
                        obj.loglikelihoodVar(tempid)=var(loglikelihood);%although this vector contains log domain values, this should be ok for how it is used
                        obj.loglikelihoodMce(tempid)=mcmcInterface.samples.computeMCE(mceNumBatches,loglikelihood);%although this vector contains log domain values, this should be ok for how it is used
                        
                        if printStatus
                            fprintf(stdout,'\tFor chainid=%d, tempid=%d, loglikelihood_mean=[%f], loglikelihood_var=[%f], loglikelihood_mce=[%f]\n', ...
                                chainid, tempid, obj.loglikelihoodMean(tempid), obj.loglikelihoodVar(tempid), obj.loglikelihoodMce(tempid));
                        end
                    catch
                        fprintf('Caught an error in computeLogLikelihood for model type=%s, subtype=%s, tempid=%d, chainid=%d\n',obj.model.type,obj.model.subtype,tempid,chainid);
                    end
                    
                    %retobj.simulations(tempid,1).hOsInterfaces.checkForKillFile();%for lengthy computations
                end
            end
            retobj=obj;
        end                
    end
end

