# powerposterior

**Warning: powerposterior is in the pre-release stage and is still being refined**

Sequence that implements the power-posterior (Friel et al., 2008) method for a group of simulations.

# Dependencies

* [mcmcBayes](https://gitlab.com/mcmcbayes/mcmcbayes.git)
* [os-agnostic-utils](https://gitlab.com/reubenajohnston/os-agnostic-utils.git)
* [mcmcInterface](https://gitlab.com/mcmcbayes/mcmcinterface.git)

# Installation
1. Clone this repository into the mcmcbayes/sequences/powerposterior folder
1. Update MATLAB path to add folders in sequences/powerposterior/code/matlab)
    ```
	>> addpath C:\opt\users\johnsra2\sandbox\mcmcbayes\sequences\powerposterior\code\matlab
	```

# Usage
1. Run a study that uses a supporting model (e.g., [regression](https://gitlab.com/mcmcbayes/regression.git))
